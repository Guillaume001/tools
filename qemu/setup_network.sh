#!/bin/bash

session="qemu-user"
tmux new-session -d -s $session

window=0
tmux rename-window -t $session:$window 'unshare'
tmux send-keys -t $session:$window 'unshare --user --map-root-user --net --mount' C-m
tmux send-keys -t $session:$window 'echo $$ > "/tmp/pid_of_namespace_for_vm"' C-m

sleep 2

window=1
tmux new-window -t $session:$window -n 'slirp4ns'
tmux send-keys -t $session:$window 'slirp4netns --configure "$(cat /tmp/pid_of_namespace_for_vm)" tap-slirp --disable-host-loopback' C-m

window=2
tmux new-window -t $session:$window -n 'network'
tmux send-keys -t $session:$window 'nsenter -t $(cat /tmp/pid_of_namespace_for_vm) -U --preserve-credentials -n -m' C-m
tmux send-keys -t $session:$window 'ip link add name br-user type bridge' C-m
tmux send-keys -t $session:$window 'ip link set dev br-user up' C-m
tmux send-keys -t $session:$window 'ip link set tap-slirp master br-user' C-m
tmux send-keys -t $session:$window 'ip route delete default dev tap-slirp' C-m
tmux send-keys -t $session:$window 'ip route delete 10.0.2.0/24 dev tap-slirp' C-m
tmux send-keys -t $session:$window 'ip route add default dev br-user' C-m

window=3
tmux new-window -t $session:$window -n 'vm'
tmux send-keys -t $session:$window 'nsenter -t $(cat /tmp/pid_of_namespace_for_vm) -U --preserve-credentials -n -m' C-m

tmux attach-session -t $session
