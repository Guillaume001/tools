#!/bin/bash

if [ ! "$#" -eq 2 ]; then
    echo "Illegal number of parameters"
    echo
    echo "Usage: $0 [QCOW2_FOLDER] [NAME]"
    echo "Example:"
    echo -e "\t $0 /tmp/foo/ bar"
    exit 1
fi


qcow_folder="$1"
name="$2"

qcow="${qcow_folder}/${name}.qcow2"
tap="$name"
mac="$(echo 00:00:00:$[RANDOM%10]$[RANDOM%10]:$[RANDOM%10]$[RANDOM%10]:$[RANDOM%10]$[RANDOM%10])"

if [ ! -d "$qcow_folder" ]; then
    echo "QCOW2_FOLDER not found"
    exit 1
fi
if [ ! -f "$qcow" ]; then
    echo "QCOW2 file not found"
    exit 1
fi

ip tuntap add $name mode tap
ip link set dev $name up
ip link set $name master br-user

monitor_folder="${qcow_folder}/monitor/"
mkdir -p "${monitor_folder}"

/usr/libexec/qemu-kvm \
	-m 4G \
	-device virtio-net,netdev=network0,mac="$mac" \
	-netdev tap,id=network0,ifname="$tap",script=no,downscript=no,vhost=on \
	-overcommit mem-lock=off \
	-smp 4,sockets=4,cores=1,threads=1 \
	-accel kvm \
	-cpu host,migratable=on \
	-sandbox on,obsolete=deny,elevateprivileges=deny,spawn=deny,resourcecontrol=deny \
	-hda "${qcow}" \
	-nographic \
	-monitor "unix:${monitor_folder}/${name},server,nowait"
